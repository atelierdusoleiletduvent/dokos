six~=1.15.0
gocardless-pro~=1.22.0
googlemaps # used in ERPNext, but dependency is defined in Frappe
ofxtools==0.8.16
plaid-python~=7.2.1
pycountry~=20.7.3
PyGithub~=1.55.0
python-stdnum~=1.16
python-youtube~=0.8.0
sepaxml~=2.4.1
taxjar~=1.9.2
tweepy~=3.10.0
Unidecode~=1.2.0
WooCommerce~=3.0.0